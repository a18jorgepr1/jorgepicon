# Instrucciones Java

## Descripción del árbol de carpetas

En la actual subcarpeta `java` del proyecto encontrarás la siguiente estructura: 

```Shell
├── README.md
├── classes
├── dist
└── src
    └── Multiplos.java
```

* En `src` se contiene el código fuente del programación
* En `classes` contendrás el bytecode generado
* En `dist` contendrás nuestro programa empaquetado en un fichero `jar`

## Ejecución del programa

El programa deberemos ejecutarlo de la siguiente forman

```Shell
java -jar Multiplos.jar <PrimerNumero> <SegundoNumero> <NumMultiplosMostrar>
```

El programa arrojará por consola todos los múltiplos del PrimerNumero o del SegundoNumero

## Documentación

Recuerda documentar tu trabajo tal y como [se ha especificado aquí](../doc/README.md)


